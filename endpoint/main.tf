terraform {
  required_providers {
    vra = {
      source = "vmware/vra"
      version = "0.3.3"
    }
  }
}  
provider "vra" {
  url           = var.url
  refresh_token = var.refresh_token
  insecure      = var.insecure
}

resource "vra_cloud_account_nsxt" "this" {
  name        = "tf-nsx-t-account"
  description = "NSX-T Endpoint Added by vRA Terraform Provider"
  username    = var.username
  password    = var.password
  hostname    = var.hostname

  accept_self_signed_cert = true

  tags {
    key   = "nsxt"
    value = "3.1.1"
  }
}
